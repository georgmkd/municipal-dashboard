import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import * as myGlobals from '../../Globals/apiroot';

export interface Login {
  token: any;

}

@Injectable()
export class AuthService {
  feedback: string;
  apiroot =  myGlobals.apirootUrl;

  constructor( private router: Router,
               private http: HttpClient) { }

  login(credentials) {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    return this.http.post<Login>(this.apiroot + '/user/login', JSON.stringify(credentials), {headers: headers, observe: 'response'})
      .map(
        ( result: HttpResponse<any>) => {
          const resHeader = result.headers;
          const resBody = result.body;
          if (result) {
            localStorage.setItem('refresh-token',  resHeader.get('refresh-token'));
            localStorage.setItem('role', resBody.type);
              if (!resBody.homeMunicipality) {
                localStorage.setItem('municipalityId', resBody.municipality.id);
                localStorage.setItem('municipalityLogo', resBody.municipality.imgurl);
                localStorage.setItem('municipalityName', resBody.municipality.name);
              } else {
                localStorage.setItem('municipalityId', resBody.homeMunicipality.id);
                localStorage.setItem('municipalityLogo', resBody.homeMunicipality.imgurl);
                localStorage.setItem('municipalityName', resBody.homeMunicipality.name);
              }
            return true;
          }
        }
      )
      .catch(this.handleError);
  }

  logout() {
    localStorage.removeItem('refresh-token');
    localStorage.removeItem('role');
    localStorage.removeItem('municipalityId');
    localStorage.removeItem('municipalityLogo');
    localStorage.removeItem('municipalityName');
    this.router.navigate(['/']);
  }

  isLoggedIn() {
    // let jwtHelper = new JwtHelper();
    const token = localStorage.getItem('refresh-token');
    if (!token) {
      return false;
    } else {
      return true;
    }
    // let expirationDate = jwtHelper.getTokenExpirationDate(token);
    // let isExpired = jwtHelper.isTokenExpired(token);
    //
    // return !isExpired;
  }

  get currentUser() {
    const role = localStorage.getItem('role');
    if (role === 'USER') {
      return true;
    }
    if (!role) {
      return null;
    }
  }

  get adminUser() {
    const role = localStorage.getItem('role');
    if (role === 'ADMIN') {
      return true;
    }
    if (!role) {
      return null;
    }
  }

  public handleError(err: HttpErrorResponse) {
    if (err.error !== undefined) {
      if (err.status.toString() === '401') {
        this.feedback = 'Invalid Credentials';
      }
    } else {
      this.feedback = 'Invalid credentials';
    }
    return Observable.throw(this.feedback);
  }

}
