import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as myGlobals from '../../Globals/apiroot';
import 'rxjs/add/operator/map';
import {PostKind} from '../../interfaces/post-kind.interface';
import {PostCategory} from '../../interfaces/post-category.interface';
import {AllPosts} from '../../interfaces/allposts.interface';
import {Tickets} from '../../interfaces/tickets.interface';
import {UpdatePost} from '../../interfaces/update-post.interface';


@Injectable()
export class AdminService {
// Apiroot
  apiroot = myGlobals.apirootUrl;
  // Get Refresh Token and Municipality ID from Local Storage
  token = localStorage.getItem('refresh-token');
  municipalityId = localStorage.getItem('municipalityId');

  constructor(private http: HttpClient) { }
  // GET all Posts
  getAllPosts() {
    const headers = new HttpHeaders({'refresh-token': this.token, 'Content-Type': 'application/json'});
    return this.http.get<AllPosts>(this.apiroot + '/admin/post/list/all', {headers: headers})
      .map(response => response);
  }
  // Get Post Details by ID
  getPostDetails(id) {

    const headers = new HttpHeaders({'Content-Type': 'application/json', 'refresh-token': this.token});
    return this.http.get<UpdatePost>(this.apiroot + '/post/details/' + id, {headers: headers})
      .map(response => response);
  }
  getPostKinds() {
    const headers = new HttpHeaders({'refresh-token': this.token, 'Content-Type': 'application/json'});

    return this.http.get<PostKind>
    (this.apiroot + '/municipality-post/kinds?id=' + this.municipalityId, {headers: headers})
      .map(response => response);
  }
  getPostCategories() {
    const headers = new HttpHeaders({'refresh-token': this.token, 'Content-Type': 'application/json'});

    return this.http.get<PostCategory>
    (this.apiroot + '/category/list', {headers: headers})
      .map(response => response);
  }
  getAllAlerts() {
    const headers = new HttpHeaders({'refresh-token': this.token, 'Content-Type': 'application/json'});

    return this.http.get<Tickets>
    (this.apiroot + '/admin/complaint/get-all', {headers: headers})
      .map(response => response);
  }

}
