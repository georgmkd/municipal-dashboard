import { Component, OnInit } from '@angular/core';
import {MapsAPILoader} from '@agm/core';
import {Router} from '@angular/router';
import {AdminService} from '../../../services/admin/admin.service';

@Component({
  selector: 'app-tickets-map',
  templateUrl: './tickets-map.component.html',
  styleUrls: ['./tickets-map.component.scss']
})
export class TicketsMapComponent implements OnInit {
  allData: any;
  lat = 41.9973462;
  lng = 21.4279956;
  constructor(
    private _loader: MapsAPILoader,
    private router: Router,
    private adminService: AdminService
  ) { }


  // GET ALL POSTS
  allAlerts() {
    return this.adminService.getAllAlerts()
      .subscribe(data => {
          this.allData = data;
        },
        err => {
          console.log(err);
          console.log('Failed');
        });
  }


  onClickInfoView(data) {
    localStorage.setItem('ticketDetails', JSON.stringify(data));
    this.router.navigate(['/admin/ticket/' + data.id]);
  }

  ngOnInit() {
    this.allAlerts();
  }

}
