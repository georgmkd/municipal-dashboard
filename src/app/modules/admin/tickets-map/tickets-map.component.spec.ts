import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsMapComponent } from './tickets-map.component';

describe('TicketsMapComponent', () => {
  let component: TicketsMapComponent;
  let fixture: ComponentFixture<TicketsMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
