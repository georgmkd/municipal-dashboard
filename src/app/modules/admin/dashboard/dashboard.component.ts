import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AllPosts} from '../../../interfaces/allposts.interface';
import * as myGlobals from '../../../Globals/apiroot';
import {AdminService} from '../../../services/admin/admin.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit, AfterViewInit {
  allData: any;
  // Apiroot
  apiroot = myGlobals.apirootUrl;
  // Get Refresh Token and Municipality ID from Local Storage
  token = localStorage.getItem('refresh-token');
  municipalityId = localStorage.getItem('municipalityId');
  displayedColumns = ['id', 'title', 'actions'];
  dataSource = new MatTableDataSource<AllPosts>(this.allData);
  resultsLength = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private http: HttpClient, private adminService: AdminService) {
    this.allData = [];
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
  }

  ngAfterViewInit(): void {
    this.allPosts();
  }
  // GET ALL POSTS
   allPosts() {
     return this.adminService.getAllPosts()
      .subscribe(data => {
        this.allData = data;
        this.dataSource = new MatTableDataSource<AllPosts>(this.allData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.resultsLength = this.allData.length;
        },
        err => {
          console.log(err);
          console.log('Failed');
        });
  }
  public archivePost(id, title: string) {


      if (confirm('Are you sure to Archive this post: "' + title + '" ?')) {
        const headers = new HttpHeaders({'refresh-token': this.token, 'Content-Type': 'application/json'});

        return this.http.get
        (this.apiroot + '/admin/archive-post/' + id, {headers: headers})
          .subscribe(data => {
              this.allPosts();
            },
            err => {
              console.log(err);
              console.log('Failed');
            });
      }
  }
}
