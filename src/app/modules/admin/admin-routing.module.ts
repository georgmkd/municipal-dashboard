import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './admin/admin.component';
import {AddPostComponent} from './add-post/add-post.component';
import {AuthService} from '../../services/auth-service/auth.service';
import {AdminGuard} from '../../guards/admin-guard';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ViewPostComponent} from './view-post/view-post.component';
import {AdminService} from '../../services/admin/admin.service';
import {AddAlertComponent} from './add-alert/add-alert.component';
import {TicketsComponent} from './tickets/tickets.component';
import {TicketDetailsComponent} from './ticket-details/ticket-details.component';
import {TicketsMapComponent} from './tickets-map/tickets-map.component';


const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminGuard],
    children: [
      {
        path: 'all-posts',
        component: DashboardComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'add-post',
        component: AddPostComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'add-alert',
        component: AddAlertComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'tickets',
        component: TicketsComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'tickets/map',
        component: TicketsMapComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'post/:id',
        component: ViewPostComponent,
        canActivate: [AdminGuard]
      },
      {
        path: 'ticket/:id',
        component: TicketDetailsComponent,
        canActivate: [AdminGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    AuthService,
    AdminService,
    AdminGuard
  ]
})
export class AdminRoutingModule { }
