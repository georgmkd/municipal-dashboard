import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AgmCoreModule} from '@agm/core';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin/admin.component';
import {AddPostComponent} from './add-post/add-post.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {MaterialModule} from '../shared/material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ViewPostComponent } from './view-post/view-post.component';
import { AddAlertComponent } from './add-alert/add-alert.component';
import { TicketsComponent } from './tickets/tickets.component';
import { TicketDetailsComponent } from './ticket-details/ticket-details.component';
import { TicketsMapComponent } from './tickets-map/tickets-map.component';


@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBq554sgC4AXqTPbNmQA44H1vkDyYXUO_k',
      language: 'en',
      libraries: ['geometry', 'places']
    }),
  ],
  declarations: [
    AdminComponent,
    AddPostComponent,
    DashboardComponent,
    ViewPostComponent,
    AddAlertComponent,
    TicketsComponent,
    TicketDetailsComponent,
    TicketsMapComponent
  ],
})
export class AdminModule { }
