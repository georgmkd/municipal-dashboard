import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as myGlobals from '../../../Globals/apiroot';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ImageResponse} from '../../../interfaces/image-upload.interface';
import {PostDetails} from '../../../models/post-details';
import {AdminService} from '../../../services/admin/admin.service';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.scss']
})
export class ViewPostComponent implements OnInit {
  postID: number;
  details: any;
  categoryData: any;
  kindData: any;
  viewEditPostForm: FormGroup;
  postDetailsModel: PostDetails;
  municipality: string;
  apiroot = myGlobals.apirootUrl;
  token = localStorage.getItem('refresh-token');
  municipalityId = localStorage.getItem('municipalityId');
  postImages: any;
  imageCollection = [];
  imageCollection2 = [];
  imageUrlCollection = [];
  postTags = [];
  checkIfUpload = false;
  checkIfUploadExist = true;

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient, private _formBuild: FormBuilder, private adminService: AdminService) {
    this.postID = this.route.snapshot.params.id;
    this.formBuildResults();
    this.postDetailsModel = new PostDetails();
  }

  getPostDetailsById() {
    return this.adminService.getPostDetails(this.postID)
      .map(response => response).subscribe(data => {
        // console.log(data);
        this.getCategories();
        this.details = data;

        // Post Details Interface
        this.postDetailsModel.postTitle = data.title;
        this.postDetailsModel.postDescript = data.description;
        this.postDetailsModel.postShortDesc = data.shortDescription;
        this.postDetailsModel.allowCommenting = data.allowCommenting;
        this.postDetailsModel.allowVoting = data.allowVoting;
        this.postDetailsModel.showToNewsfeed = data.showToNewsfeed;
        this.postDetailsModel.published = data.published;
        this.postDetailsModel.type = data.type;
        this.postDetailsModel.id = data.id;
        this.postDetailsModel.postCategory = data.postCategories[0].category.id;
        this.postDetailsModel.kinds = data.kinds[0].id;
        this.postDetailsModel.postImage = this.apiroot  + data.postImages[0].image.url;
        this.imageCollection = data.postImages;

        for (let i = 0; i < data.postImages.length; i++) {
          this.imageCollection2.push(data.postImages[i].image.id);
          this.imageUrlCollection.push(this.apiroot + data.postImages[i].image.url);
          this.viewEditPostForm.addControl('postImages', this._formBuild.array([]));
          (this.viewEditPostForm.controls.postImages as FormArray).push(this._formBuild.group({
            image : this._formBuild.group({ id: data.postImages[i].image.id})
          }));
        }
        ((this.viewEditPostForm.controls.postTags as FormArray).controls[0] as FormGroup).removeControl('tag');
        (this.viewEditPostForm.controls.postTags as FormArray).controls.splice(0, 1);
        for (let i = 0; i < data.postTags.length; i++) {
          this.postTags.push(data.postTags[i].tag.name);
          this.postTags.push(this.apiroot + data.postTags[i].tag.name);
          this.viewEditPostForm.addControl('postTags', this._formBuild.array([]));
          (this.viewEditPostForm.controls.postTags as FormArray).push(this._formBuild.group({
            tag : this._formBuild.group({ name: data.postTags[i].tag.name})
          }));
        }

        // Municipality Details
        this.postDetailsModel.municipalityName = data.municipality.name;
        this.postDetailsModel.municipalityLogo = this.apiroot  + data.municipality.imgurl;
      });
  }

  formBuildResults() {
    this.viewEditPostForm = this._formBuild.group({
      id: null,
      title: [null, [Validators.required, Validators.minLength(2)]],
      description: [null, [Validators.required, Validators.minLength(2)]],
      shortDescription: [null, [Validators.required, Validators.minLength(2)]],
      postCategories:  this._formBuild.array([this.addCategory()]),
      postTags: this._formBuild.array([this.addPostTags()]),
      kinds: this._formBuild.array([this.addKind()]),
      allowCommenting: false,
      allowVoting: false,
      showToNewsfeed: false,
      published: false,
      type: [null, Validators.required]
    });
  }
  addPostTags() {
    const group = this._formBuild.group({
      tag: this._formBuild.group({ name: ['']})
    });
    return group;
  }
  addTagName() {
    const control: FormArray = this.viewEditPostForm.get(`postTags`) as FormArray;
    control.push(this.addPostTags());
  }
  addCategory() {
    const group = this._formBuild.group({
      category: this._formBuild.group({ id: ['']})
    });
    return group;
  }
  addKind() {
    const group = this._formBuild.group({
      id: ['']
    });
    return group;
  }

  ngOnInit() {
    this.getPostDetailsById();
    this.getKinds();
  }
  public archivePost(id, title: string) {
    if (confirm('Are you sure to Archive this post: "' + title + '" ?')) {
      const headers = new HttpHeaders({'refresh-token': this.token, 'Content-Type': 'application/json'});
      return this.http.get
      (this.apiroot + '/admin/archive-post/' + id, {headers: headers})
        .subscribe(data => {
            this.router.navigate(['/admin/all-posts']);
          },
          err => {
            console.log(err);
            console.log('Failed');
          });
    }
  }
  removeTag(x) {
    (this.viewEditPostForm.controls.postTags as FormArray).controls.splice(x, 1);
    this.viewEditPostForm.addControl('postTags', this._formBuild.array([]));
  }
  // GET post categories
  private getCategories() {
    return this.adminService.getPostCategories()
      .subscribe(data => this.categoryData = data,
        err => {
          console.log(err);
          console.log('Failed');
        });
  }
  // GET post kinds
  private getKinds() {
    return this.adminService.getPostKinds()
      .subscribe(data => this.kindData = data,
        err => {
          console.log(err);
          console.log('Failed');
        });
  }

  // UPDATE POST
   editPost() {
    const headers = new HttpHeaders({'Content-Type': 'application/json', 'refresh-token': this.token});
    this.http.post(this.apiroot + '/admin/post/update', JSON.stringify(this.viewEditPostForm.value), {
      headers: headers
    })
      .subscribe(
        (data) => {
          this.router.navigate(['/admin/all-posts']);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  removeImage(x) {
    console.log(x);
    // ((this.addPostForm.controls.postImages as FormArray).controls[x] as FormGroup).removeControl('image');
    this.imageCollection.splice(x, 1);
    this.imageCollection2.splice(x, 1);
    (this.viewEditPostForm.controls.postImages as FormArray).controls.splice(x, 1);
    this.postDetailsModel.postImages.splice(x, 1);
    this.viewEditPostForm.addControl('postImages', this._formBuild.array([]));
    this.imageUrlCollection.splice(x, 1);
    this.postDetailsModel.postImage = this.imageUrlCollection[0];
  }
  removeImageExisting(x) {
    this.imageCollection.splice(x, 1);
    this.imageCollection2.splice(x, 1);
    this.imageUrlCollection.splice(x, 1);
    this.postDetailsModel.postImage = this.imageUrlCollection[0];
    console.log(x);
    (this.viewEditPostForm.controls.postImages as FormArray).controls.splice(x, 1);
    this.viewEditPostForm.addControl('postImages', this._formBuild.array([]));
  }

  // Upload Multiple Files
  uploadMultipleFiles(event) {

    // Create the variables
    const multipleFiles = event.target.files;
    const formData = new FormData();
    const headers = new HttpHeaders({'refresh-token': this.token});


    // Push images in the empty array
    for (let i = 0; i < multipleFiles.length; i++) {
      this.imageCollection.push(multipleFiles[i]);
    }

    // Append the files in the form data
    for (let x = 0; x < this.imageCollection.length; x++) {
      formData.append('files', this.imageCollection[x]);
    }

    // Upload Images
    this.http.post<ImageResponse>
    (this.apiroot + '/files/upload', formData, {headers: headers}).subscribe(
      data => {
        this.checkIfUploadExist = false;
        this.viewEditPostForm.removeControl('postImages');
        this.postDetailsModel.postImages = data.successFiles;
        (<HTMLInputElement>document.getElementById('multipleFiles')).value = '';
        this.imageCollection = [];
        this.imageCollection2 = [];
        this.imageUrlCollection = [];
        this.checkIfUpload = true;
        this.postDetailsModel.postImage = this.apiroot + this.postDetailsModel.postImages[0].url;
        for (let i = 0; i < this.postDetailsModel.postImages.length; i++) {
          this.imageCollection2.push(this.postDetailsModel.postImages[i].id);
          this.imageUrlCollection.push(this.apiroot + this.postDetailsModel.postImages[i].url);
          this.viewEditPostForm.addControl('postImages', this._formBuild.array([]));
          (this.viewEditPostForm.controls.postImages as FormArray).push(this._formBuild.group({
            image : this._formBuild.group({ id: this.postDetailsModel.postImages[i].id})
          }));
        }
      },
      err => {
        console.log(err);
        alert('Error');
      }
    );
  }

}
