import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormArray, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as myGlobals from '../../../Globals/apiroot';
import {ImageResponse} from '../../../interfaces/image-upload.interface';
import {AdminService} from '../../../services/admin/admin.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
  // Apiroot
  apiroot = myGlobals.apirootUrl;
  addPostForm: FormGroup
  categoryData: any;
  kindData: any;
  firstImage: string;
  // Images Variables
  imageCollection = [];
  imageUrlCollection = [];
  postImages: any;
  // Get Refresh Token and Municipality ID from Local Storage
  token = localStorage.getItem('refresh-token');
  municipalityId = localStorage.getItem('municipalityId');
  municipalityLogo: string = this.apiroot +  localStorage.getItem('municipalityLogo');
  municipalityName: string = localStorage.getItem('municipalityName');


  constructor(private _formBuild: FormBuilder, private http: HttpClient, private adminService: AdminService) {
    this.formBuildResults();
  }

  ngOnInit() {
    this.formBuildResults();
    this.getKinds();
    this.getCategories();
  }

  formBuildResults() {
    this.addPostForm = this._formBuild.group({
      title: [null, [Validators.required, Validators.minLength(2)]],
      description: '',
      shortDescription: '',
      format: 'SIMPLE',
      issued: true,
      postCategories: this._formBuild.array([this.addCategory()]),
      kinds: this._formBuild.array([this.addKind()]),
      postTags: this._formBuild.array([this.addPostTags()]),
      allowCommenting: false,
      allowVoting: false,
      showToNewsfeed: false,
      published: false,
      postImages: [null, Validators.required, Validators.minLength(2)],
      type: [null, Validators.required]
    });
  }
  addCategory() {
    const group = this._formBuild.group({
      category: this._formBuild.group({ id: ['']})
    });
    return group;
  }
  addPostTags() {
    const group = this._formBuild.group({
      tag: this._formBuild.group({ name: ['']})
    });
    return group;
  }
  addTagName() {
    const control: FormArray = this.addPostForm.get(`postTags`) as FormArray;
    control.push(this.addPostTags());
  }
  addKind() {
    const group = this._formBuild.group({
      id: ['']
    });
    return group;
  }
  removeImage(x) {
    // ((this.addPostForm.controls.postImages as FormArray).controls[x] as FormGroup).removeControl('image');
    this.imageCollection.splice(x, 1);
    (this.addPostForm.controls.postImages as FormArray).controls.splice(x, 1);
    this.postImages.splice(x, 1);
    this.addPostForm.addControl('postImages', this._formBuild.array([]));
    this.imageUrlCollection.splice(x, 1);
    this.firstImage = this.imageUrlCollection[0];
  }
  removeTag(x) {
    (this.addPostForm.controls.postTags as FormArray).controls.splice(x, 1);
    this.addPostForm.addControl('postTags', this._formBuild.array([]));
  }
  // GET post kinds
  private getKinds() {
    return this.adminService.getPostKinds()
      .subscribe(data => this.kindData = data,
        err => {
          console.log(err);
          console.log('Failed');
        });
  }

  // GET post categories
  private getCategories() {
    return this.adminService.getPostCategories()
      .subscribe(data => this.categoryData = data,
        err => {
          console.log(err);
          console.log('Failed');
        });
  }
  addPost() {
    const headers = new HttpHeaders({'Content-Type': 'application/json', 'refresh-token': this.token});
    this.http.post(this.apiroot + '/admin/post/create', JSON.stringify(this.addPostForm.value), {
      headers: headers
    })
      .subscribe(
        (data) => {
          alert('Succesfull Creation of Post');
          this.addPostForm.reset();
          this.postImages = [];
          this.firstImage = '';
        },
        (err) => {
          console.log(err);
        }
      );
  }
  // Upload Multiple Files
  uploadMultipleFiles(event) {

    // Create the variables
    const multipleFiles = event.target.files;
    const formData = new FormData();
    const headers = new HttpHeaders({'refresh-token': this.token});


    // Push images in the empty array
    for (let i = 0; i < multipleFiles.length; i++) {
      this.imageCollection.push(multipleFiles[i]);
    }

    // Append the files in the form data
    for (let x = 0; x < this.imageCollection.length; x++) {
      formData.append('files', this.imageCollection[x]);
    }

    // Upload Images
    this.http.post<ImageResponse>
    (this.apiroot + '/files/upload', formData, {headers: headers}).subscribe(
      data => {
        this.addPostForm.removeControl('postImages');
        this.postImages = data.successFiles;
        (<HTMLInputElement>document.getElementById('multipleFiles')).value = '';
        this.imageCollection = [];
        this.firstImage = this.apiroot + this.postImages[0].url;
        for (let i = 0; i < this.postImages.length; i++) {
          this.imageCollection.push(this.postImages[i].id);
          this.imageUrlCollection.push(this.apiroot + this.postImages[i].url);
          this.addPostForm.addControl('postImages', this._formBuild.array([]));
          (this.addPostForm.controls.postImages as FormArray).push(this._formBuild.group({
             image : this._formBuild.group({ id: this.postImages[i].id})
            }));
        }
        console.log(this.imageCollection);
      },
      err => {
        console.log(err);
        alert('Error');
      }
    );
  }

}
