import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth-service/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {
  isNavbarCollapsed = true;

  events: string[] = [];
  opened: boolean;

  constructor(private authService: AuthService) { }


}
