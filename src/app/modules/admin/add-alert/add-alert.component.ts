import {Component, Input, NgZone, OnInit} from '@angular/core';
import {AgmCoreModule, MouseEvent, MapsAPILoader} from '@agm/core';
import {} from '@types/googlemaps';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as myGlobals from '../../../Globals/apiroot';
import {Router} from '@angular/router';
import {Marker} from '../../../interfaces/marker.interface';

@Component({
  selector: 'app-add-alert',
  templateUrl: './add-alert.component.html',
  styleUrls: ['./add-alert.component.scss']
})
export class AddAlertComponent implements OnInit {
// Input for marker draggable
  @Input('markerDraggable') draggable = true;

  // Starting coordinates - MKD;
  lat = 41.594726;
  lng = 21.255308;

  // Interface variables
  // title: string;
  // description: string;
  // location: string;
  newLat: number;
  newLon: number;

  loc: string;

  // Apiroot
  apiroot = myGlobals.apirootUrl;

  addAlertForm: FormGroup;
  token = localStorage.getItem('refresh-token');

  constructor(
    private _loader: MapsAPILoader,
    private ngZone: NgZone,
    private _formBuild: FormBuilder,
    private http: HttpClient,
    private router: Router
  ) {
    this.formBuildResults();
  }
  formBuildResults() {
    this.addAlertForm = this._formBuild.group({
      title: [null, [Validators.required, Validators.minLength(2)]],
      description: [null, [Validators.required, Validators.minLength(2)]],
      location: '',
      lat: '',
      lon: ''
    });
  }
  ngOnInit() {
    this.autocomplete();
  }

  addAlert() {
    const headers = new HttpHeaders({'Content-Type': 'application/json', 'refresh-token': this.token});
    this.http.post(this.apiroot + '/alert/create', JSON.stringify(this.addAlertForm.value), {
      headers: headers
    })
      .subscribe(
        (data) => {
          alert('Succesfull Creation of Post');
          this.addAlertForm.reset();
        },
        (err) => {
          console.log(err);
        }
      );
  }

  // Draggable marker
  markerDragEnd(m: Marker, $event: MouseEvent) {
    // console.log('dragEnd', m, $event);
    // console.log('lat: ' + this.newLat, 'lon: ' + this.newLon);


    // Change starting coordinates to current dragged lat and lng;
    m.lat = $event.coords.lat;
    m.lng = $event.coords.lng;
    m.draggable = true;

    // Save new coordinates
    this.newLat = m.lat;
    this.newLon = m.lng;


    // Reverse geocode lat and lng to address
    let latlng = new google.maps.LatLng(this.newLat, this.newLon);
    let geocoder = new google.maps.Geocoder();
    // geocoder.geocode({ latLng: latlng }, function (results, status) {
    //   if (status == google.maps.GeocoderStatus.OK) {
    //     if (results[0]) {
    //       (<HTMLInputElement>document.getElementById('alert-location')).value = results[0].formatted_address;
    //       console.log(results[0].formatted_address);
    //     }
    //   }
    // });
  }
  // Autocomplete geolocation
  autocomplete() {
    this._loader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(<HTMLInputElement>document.getElementById('alert-location'), {});
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        this.ngZone.run(() => {
          const place = autocomplete.getPlace();
          this.loc = autocomplete.getPlace().formatted_address;
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.newLat = place.geometry.location.lat();
          this.newLon = place.geometry.location.lng();
        });
      });
    });
  }
}
