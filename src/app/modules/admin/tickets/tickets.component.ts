import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import * as myGlobals from '../../../Globals/apiroot';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Tickets} from '../../../interfaces/tickets.interface';
import {AdminService} from '../../../services/admin/admin.service';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit,  AfterViewInit {

  allData: any;
  // Apiroot
  apiroot = myGlobals.apirootUrl;
  // Get Refresh Token and Municipality ID from Local Storage
  token = localStorage.getItem('refresh-token');
  municipalityId = localStorage.getItem('municipalityId');
  displayedColumns = ['id', 'complaintName', 'location', 'status', 'priority', 'complaining', 'actions'];
  dataSource = new MatTableDataSource<Tickets>(this.allData);
  resultsLength = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private http: HttpClient, private adminService: AdminService) {
    this.allData = [];
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  viewTickets(data) {
    localStorage.setItem('ticketDetails', JSON.stringify(data));
    console.log(JSON.stringify(data));
  }

  // GET ALL POSTS
  allAlerts() {
    return this.adminService.getAllAlerts()
      .subscribe(data => {
          this.allData = data;
          this.dataSource = new MatTableDataSource<Tickets>(this.allData);
          this.dataSource.filterPredicate = (data, filter: string)  => {
            const accumulator = (currentTerm, key) => {
              return key === 'complaintCategory' ? currentTerm + data.complaintCategory.description : currentTerm + data[key];
            };
            const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
            // Transform the filter by converting it to lowercase and removing whitespace.
            const transformedFilter = filter.trim().toLowerCase();
            return dataStr.indexOf(transformedFilter) !== -1;
          };
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.resultsLength = this.allData.length;
        },
        err => {
          console.log(err);
          console.log('Failed');
        });
  }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    this.allAlerts();
  }

}
