import {Component, NgZone, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as myGlobals from '../../../Globals/apiroot';
import {MapsAPILoader} from '@agm/core';

export interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

@Component({
  selector: 'app-ticket-details',
  templateUrl: './ticket-details.component.html',
  styleUrls: ['./ticket-details.component.scss']
})
export class TicketDetailsComponent implements OnInit {
  postID: number;
  dataResult = JSON.parse(localStorage.getItem('ticketDetails'));

  lat = this.dataResult.lat;
  lng = this.dataResult.lon;

  apiroot = myGlobals.apirootUrl;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _loader: MapsAPILoader,
    private ngZone: NgZone
  ) {
    this.postID = this.route.snapshot.params.id;
  }

  ngOnInit() {

  }

}
