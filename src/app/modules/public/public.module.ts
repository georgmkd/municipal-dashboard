import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewPostComponent } from './view-post/view-post.component';
import {PublicRoutingModule} from './public-routing.module';


@NgModule({
  imports: [
    CommonModule,
    PublicRoutingModule,
  ],
  declarations: [
  ViewPostComponent],
})
export class PublicModule { }
