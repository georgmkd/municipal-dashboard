import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ViewPostComponent} from './view-post/view-post.component';
import {AdminService} from '../../services/admin/admin.service';



const routes: Routes = [
  {
    path: 'view-post/:id',
    component: ViewPostComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    AdminService
  ]
})
export class PublicRoutingModule { }
