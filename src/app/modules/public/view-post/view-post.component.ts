import { Component, OnInit } from '@angular/core';
import * as myGlobals from '../../../Globals/apiroot';
import {ActivatedRoute} from '@angular/router';
import {AdminService} from '../../../services/admin/admin.service';

export class PostDetails {
  municipalityName: string;
  municipalityLogo: string;
  postTitle: string;
  postDescript: string;
  postImage: string;
  creationDate: string;
}

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.scss']
})
export class ViewPostComponent implements OnInit {
  // Post ID and apiroot
  postID: number;
  apiroot = myGlobals.apirootUrl;

  postDetailsModel: PostDetails;

  constructor(private route: ActivatedRoute, private postDetails: AdminService) {
    this.postID = this.route.snapshot.params.id;
    this.postDetailsModel = new PostDetails();
  }

  getPostDetailsById() {
    this.postDetails.getPostDetails(this.postID)
      .subscribe((data: any) => {

        // Post Details Interface
        this.postDetailsModel.postTitle = data.title;
        this.postDetailsModel.postDescript = data.description;
        this.postDetailsModel.postImage = this.apiroot  + data.postImages[0].image.url;

        // Municipality Details
        this.postDetailsModel.municipalityName = data.municipality.name;
        this.postDetailsModel.municipalityLogo = this.apiroot  + data.municipality.imgurl;
        this.postDetailsModel.creationDate = data.creationDate;

        console.log(data.creationDate);
      });
  }

  ngOnInit() {
    this.getPostDetailsById();
  }

}
