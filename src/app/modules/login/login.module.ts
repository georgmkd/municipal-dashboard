

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import {LoginRoutingModule} from './login-routing.module';
import {FormsModule} from '@angular/forms';
import {AuthService} from '../../services/auth-service/auth.service';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from '../shared/material/material.module';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    LoginRoutingModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [LoginComponent],
  providers: [
    AuthService
  ]
})
export class LoginModule { }
