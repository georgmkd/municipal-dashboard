import { Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../../services/auth-service/auth.service';
import { Login } from '../../../interfaces/login.interface';

declare var Fingerprint2: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  hash;
  invalidLogin: boolean;
  displayError: string;

  login: Login = {username: '', password: '', deviceId: ''};

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService) {
    new Fingerprint2().get((result, components) => {
      this.hash = result;
      this.login.deviceId = result;  // a hash, representing your device fingerprint
    });
  }
  signIn() {
    this.authService.login(this.login)
      .subscribe((result: any) => {
          if (result) {
            const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl');
            this.router.navigate([returnUrl || '/admin/all-posts']);
          } else {
            this.invalidLogin = true;
          }
        },
        error => {
          this.displayError = error;
          this.invalidLogin = true;
        },
        () => console.log('Successfully logged in'));
  }

}
