import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PublicGuard} from './guards/public-guard';
import {AdminGuard} from './guards/admin-guard';
import {ViewPostComponent} from './modules/public/view-post/view-post.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/modules/login/login.module#LoginModule',
    canActivate: [PublicGuard],
    // data: { state: 'login' }
  },
  {
    path: 'view-post-details/:id',
    component: ViewPostComponent
  },
  {
    path: 'admin',
    loadChildren: 'app/modules/admin/admin.module#AdminModule',
    canActivate: [PublicGuard],
    // data: { state: 'login' }
  },
  {
    path: 'public',
    loadChildren: 'app/modules/public/public.module#PublicModule',
    canActivate: [PublicGuard],
    // data: { state: 'login' }
  },
  {
    path: 'admin',
    redirectTo: 'login',
    canActivate: [AdminGuard],
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


