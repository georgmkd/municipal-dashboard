export class PostDetails {
  id: number;
  municipalityName: string;
  municipalityLogo: string;
  postTitle: string;
  postDescript: string;
  postShortDesc: string;
  postCategory: number;
  postImage: string;
  postImages: any;
  kinds: any;
  allowCommenting: boolean;
  allowVoting: boolean;
  showToNewsfeed: boolean;
  published: boolean;
  type: string;
}
