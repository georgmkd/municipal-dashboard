export class AddPostModel {
  id: number;
  title: string;
  description: string;
  shortDescription: string;
  format: string;
  issued: boolean;
  postCategories: Category[];
  postImages: PostImages[];
  postTags: PostTags[];
  kinds: KindID[];
  allowCommenting: boolean;
  allowVoting: boolean;
  showToNewsfeed: boolean;
  published: boolean;
  type: string;
}
export class Category {
  category: CategoryID;
}
export class CategoryID {
  id: number;
}
export class KindID {
  id: number;
}
export class PostImages {
  image: PostImageID;
}
export class PostImageID {
  id: number;
}
export class PostTags {
  tag: TagName;
}
export class TagName {
  name: string;
}
