export interface Tickets {
  id: number;
  location: string;
  municipality: MunicipalityName;
  complaintStatus: string;
  priority: string;
  complaintCategory: ComplaintDescription;
  complaining: number;

}
export interface MunicipalityName {
  name: string;
}
export interface ComplaintDescription {
  description: string;
}
