
export interface AllPosts {
  id: number;
  title: string;
  description: string;
}
