export interface PostCategory {
  postId: number;
  postCategory: string;
}
