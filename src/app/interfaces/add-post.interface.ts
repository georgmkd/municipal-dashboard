export interface AddPost {
  title: string;
  description: string;
  shortDescription: string;
  format: string;
  issued: boolean;
  postCategories: Category[];
  postImages: PostImages[];
  postTags: PostTags[];
  kinds: KindID[];
  allowCommenting: boolean;
  allowVoting: boolean;
  showToNewsfeed: boolean;
  published: boolean;
  type: string;
}
export interface Category {
  category: CategoryID;
}
export interface CategoryID {
  id: number;
}
export interface KindID {
  id: number;
}
export interface PostImages {
  image: PostImageID;
}
export interface PostImageID {
  id: number;
}
export interface PostTags {
  tag: TagName;
}
export interface TagName {
  name: string;
}
