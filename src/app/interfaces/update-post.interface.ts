export interface UpdatePost {
  id: number;
  title: string;
  description: string;
  shortDescription: string;
  format: string;
  issued: boolean;
  postCategories: Category;
  postImages: PostImages[];
  postTags: PostTags[];
  kinds: KindID[];
  allowCommenting: boolean;
  allowVoting: boolean;
  showToNewsfeed: boolean;
  published: boolean;
  type: string;
  municipality: Municipality;
}
export interface Municipality {
  id: number;
  imgurl: string;
  name: string;
}
export interface Category {
  category: CategoryID;
}
export interface CategoryID {
  id: number;
  name: string;
}
export interface KindID {
  id: number;
}
export interface PostImages {
  image: PostImageID;
}
export interface PostImageID {
  url: string;
  id: number;
}
export interface PostTags {
  tag: TagName;
}
export interface TagName {
  name: string;
}
