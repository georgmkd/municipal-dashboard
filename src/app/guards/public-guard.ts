import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
// import {AuthService} from './auth.service';

@Injectable()
export class PublicGuard implements CanActivate {

  constructor(
    private router: Router
  ) { }

  canActivate() {
    // let user = this.authService.adminUser;
    // if (user) return true;
    // this.router.navigate(['/no-access']);
    return true;
  }

}
